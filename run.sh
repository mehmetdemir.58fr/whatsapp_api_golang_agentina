#!/bin/sh

# kill prev copy if exists
pkill -e -f go-whatsapp-proxy


# start a new one
nohup /home/whatsmeow-proxy/go-whatsapp-proxy &

